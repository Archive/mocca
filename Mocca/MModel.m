/* MModel
 * Copyright (C) 2004 GNOME Foundation
 *
 * AUTHORS:
 *      �lvaro Pe�a <alvaropg@telefonica.net>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <Mocca/Models/MModel.h>

#include <objc/objc-api.h>
#include <glib.h>

@implementation MModel

- (id) getKey: (char *) key
{
	SEL   selector;
	char *functionKey;
	
	/* First [object key] */
	functionKey = g_strdup (key);
	selector = sel_get_any_uid (key);
	if ([self respondsTo: selector] == YES) {
		return [self perform: selector];
	}
}

- (void) setKey: (char *) key
      withValue: (id) value
{
	SEL   selector;
	IMP   key_impl;
	char *functionKey;

	/* First with [object setKey: value] */
	functionKey = g_strdup (key);
	functionKey[0] = g_ascii_toupper (functionKey[0]);
	functionKey = g_strdup_printf ("set%s:", functionKey);
	selector = sel_get_any_uid (functionKey);
	if (!selector) {
		g_warning ("This object don't respond to %s", functionKey);
		return;
	}
	key_impl = [self methodFor: selector];
	if (key_impl) {
		key_impl (self, selector, value);
	}
}

@end
