/* MInterfaceModel
 * Mocca Interface Model
 * This object storage the information about the conection between
 * an interface and the code, like delegates, outlets and signals
 * connections
 *
 * Copyright (C) 2004 GNOME Foundation
 *
 * AUTHORS:
 *      �lvaro Pe�a <alvaropg@telefonica.net>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <Mocca/Models/MInterfaceModel.h>

#include <glib.h>

@implementation MInterfaceModel

- (id) init
{
	if (self = [super init]) {
		_gladeXML = NULL;
		_delegates = NULL;
		_outlets = NULL;
		_events = NULL;
		
		return self;
	} else {
		return NULL;
	}
}

- (gchar *) gladeUri
{
	return _gladeUri;
}

- (void) setGladeUri: (gchar *) uri
{
	if (uri != NULL) {
		if (_gladeUri) {
			g_free (_gladeUri);
		}
		
		_gladeUri = g_strdup (uri);
	}
}

- free
{
	if (_gladeUri) {
		g_free (_gladeUri);
		_gladeUri = NULL;
	}

	if (_delegates) {
		g_list_free (_delegates);
		_delegates = NULL;
	}
	
	

	[super free];
}

- (void) appendDelegate: (MDelegateModel *) dModel
{
	if (dModel != NULL) {
		_delegates = g_list_append (_delegates, dModel);
	}
}

- (void) appendOutlet: (MOutletModel *) oModel
{
	if (oModel != NULL) {
		_outlets = g_list_append (_outlets, oModel);
	}
}

- (void) appendEvent: (MEventModel *) eModel
{
	if (eModel != NULL) {
		_events = g_list_append (_events, eModel);
	}
}


@end
