/* MInterfaceController
 * Mocca Interface Controller
 * This object load all the information of a interface
 *
 * Copyright (C) 2005 GNOME Foundation
 *
 * AUTHORS:
 *      �lvaro Pe�a <alvaropg@telefonica.net>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <Mocca/Controllers/MInterfaceController.h>
#include <Mocca/Views/MView.h>

#include <glib.h>
#include <glade/glade.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

static GHashTable *toObjectHash = NULL;

@implementation MInterfaceController

GladeXML* loadGladeElement (xmlNode *node, xmlDoc *doc)
{
	xmlChar   *key;
	xmlNode   *currentNode = NULL;
	GladeXML  *gladeXml = NULL;
	GtkWidget *widget;
	Class      fromObject;
	Class      toObject;
	
	currentNode = node->xmlChildrenNode;
	
	while (currentNode) {
		if (xmlStrcmp (currentNode->name, (const xmlChar *) "uri") == 0) {
			key = xmlNodeListGetString (doc, currentNode->xmlChildrenNode, 1);
			gladeXml = glade_xml_new (key, NULL, NULL);
			xmlFree (key);
		} else if (xmlStrcmp (currentNode->name, (const xmlChar *) "event") == 0) {
			gchar *event_name;
			SEL selector;
			IMP key_impl;

			/* not used */
			key = xmlGetProp (currentNode, "fromObjectClass");
			xmlFree (key);

			/* the widget */
			key = xmlGetProp (currentNode, "fromObjectName");
			widget = glade_xml_get_widget (gladeXml, key);
			xmlFree (key);

			/* event to call */
			key = xmlGetProp (currentNode, "fromObjectMethod");
			event_name = g_strdup (key);
			xmlFree (key);

			/* responder object and function method */
			if (!toObjectHash) {
				toObjectHash = g_hash_table_new (g_str_hash, g_str_equal);
			}
			key = xmlGetProp (currentNode, "callObject");
			toObject = g_hash_table_lookup (toObjectHash, key);
			if (!toObject) {
				toObject = objc_lookup_class (key);
				toObject = [[toObject alloc] init];
				g_hash_table_insert (toObjectHash, key, toObject);
			}
			xmlFree (key);

			key = xmlGetProp (currentNode, "callObjectMethod");

			selector = sel_get_any_uid (key);
			key_impl = [toObject methodFor: selector];
			if (key_imp) {
			}

			xmlFree (key);

			

			g_free (event_name);
		} else if (xmlStrcmp (currentNode->name, (const xmlChar *) "delegate") == 0) {
			/* From Object */
			/* One object only can delegate in one time, no hash needed */
			
			/* class name */
			key = xmlGetProp (currentNode, "fromObjectClass");
			fromObject = objc_lookup_class (key);
			fromObject = [[fromObject alloc] init];
			xmlFree (key);

			/* object name */
			key = xmlGetProp (currentNode, "fromObjectName");
			widget = glade_xml_get_widget (gladeXml, key);
			[fromObject setView: (id) widget];
			xmlFree (key);

			/* To Object */
			/* a hash save the objects created some time */
			if (!toObjectHash) {
				toObjectHash = g_hash_table_new (g_str_hash, g_str_equal);
			}

			key = xmlGetProp (currentNode, "delegateObject");
			toObject = g_hash_table_lookup (toObjectHash, key);
			if (!toObject) {
				toObject = objc_lookup_class (key);
				toObject = [[toObject alloc] init];
				g_hash_table_insert (toObjectHash, key, toObject);
			}
			
			[fromObject setDelegate: (id) toObject];
			xmlFree (key);
		}
		
		currentNode = currentNode->next;
	}

	return gladeXml;
}

GList* loadMicElements (xmlNode *node, xmlDoc *document)
{
	xmlNode  *currentNode = NULL;
	GList    *gladeList  = NULL;
	GladeXML *gladeXml;

	currentNode = node;
	while (currentNode != NULL) {
		if ((xmlStrcmp (currentNode->name, (const xmlChar *) "model")) == 0) {
			gladeXml = loadGladeElement (currentNode, document);
			gladeList = g_list_append (gladeList, gladeXml);
		}
		currentNode = currentNode->next;
	}
	
	return gladeList;
}

- (id) init
{
	if (self = [super init]) {
		_modelList = NULL;
		return self;
	} else {
		return NULL;
	}

}

- (MInterfaceController *) initWithUri: (gchar *) uri
{
	if (uri == NULL) {
		return NULL;
	}
	
	self = [self init];
	if (!self) {
		return NULL;
	}
	
	if (![self loadUri: uri]) {
		return NULL;
	}
			
	return self;
}

- (gboolean) loadUri: (gchar *) uri
{
	xmlDoc  *doc = NULL;
	xmlNode *rootElement = NULL;

	if (uri == NULL) {
		return FALSE;
	}
	
	if (_micUri) {
		g_free (_micUri);
		_micUri = FALSE;
	}

	_micUri = g_strdup (uri);

	doc = xmlReadFile (_micUri, NULL, 0);
	if (doc == NULL) {
		printf ("MInterfaceController error: could not parse file %s\n", uri);
		return FALSE;
		
	}
	
	rootElement = xmlDocGetRootElement(doc);
	if (xmlStrcmp (rootElement->name, (const xmlChar *) "mic") == 0) {
		_modelList = (id) loadMicElements (rootElement->xmlChildrenNode, doc);
		xmlFreeDoc (doc);
		return TRUE;
	} else {
		return FALSE;
	}
}

- free
{
	if (_modelList) {
		g_list_free ((GList *) _modelList);
	}

	if (_micUri) {
		g_free (_micUri);
		_micUri = NULL;
	}	
	
	[super free];
}


@end
