/* MApplication
 * Mocca Application
 * This object load all the application information
 *
 * Copyright (C) 2005 GNOME Foundation
 *
 * AUTHORS:
 *      Álvaro Peña <apg@openshine.com>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <Mocca/MApplication.h>
#include <Mocca/Controllers/MInterfaceController.h>
#include <glib.h>

gint
MApplicationMain (const gchar *appId, const gchar *appVersion, gint nArgs, gchar *args[], const gchar *micPath)
{
	MInterfaceController *iController;
	
	if (!g_file_test (micPath, G_FILE_TEST_EXISTS)) {
		g_printf ("Mocca can't init: mic file don't exists.\n");
		return -1;
	}
	
	gtk_init (&nArgs, &args);

	mApp = [[MApplication alloc] init];
	iController = [[MInterfaceController alloc] initWithUri: (gchar *) micPath];
	if (!iController) {
		g_printf ("Error loading mic file.\n");
		return -1;
	}
	[mApp setIController: iController];
		
	gtk_main ();

	return 0;
}

@implementation MApplication

- (id) init
{
	if (self = [super init]) {
		_iController = NULL;

		return self;
	} else {
		return NULL;
	}
}

- (void) quit
{
	[_iController free];
	gtk_main_quit ();
}

- (MInterfaceController *) iController
{
	return _iController;
}

- (void) setIController: (MInterfaceController *) iController
{
	_iController = iController;
}

@end
