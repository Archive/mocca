#include "main.h"

#include <Mocca/Models/MModel.h>
#include <glib.h>

void test_mmodel (void)
{
	MModel *model = NULL;
	
	g_print ("Starting MModel test.\n");

	g_print ("Creating the object ");
	model = [[MModel alloc] init];
	if (model != NULL) {
		g_print ("Ok.\n");
	} else {
		g_print ("Failed.\n");
		return;
	}

	g_print ("Free the object.\n");
	[model free];
}
