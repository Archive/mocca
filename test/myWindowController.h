#ifndef __MYWINDOWCONTROLLER_H__
#define __MYWINDOWCONTROLLER_H__

#include <Mocca/Mocca.h>

@interface myWindowController: Object
{
}

- (void) onWindowClose: (MWindow *) window;

@end

#endif /* __MYWINDOWCONTROLLER_H__ */
