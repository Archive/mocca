#include "myWindowController.h"
#include <glib.h>
#include <gtk/gtk.h>

@implementation myWindowController

- (id) init
{
	if (self = [super init]) {
		return self;
	} else {
		return NULL;
	}
}

- (void) onWindowClose: (MWindow *) window
{
	g_printf ("On window close.\n");

	[window destroy];
	[window free];

	[self free];
	
	gtk_main_quit ();
}

@end
