#include <glib.h>
#include <gtk/gtk.h>

#include <Mocca/Mocca.h>

void
app_init (void)
{
	MInterfaceController *iController;
	Class myClass;
	
	iController = [[MInterfaceController alloc] initWithUri: "./mabout.mic"];
	if (iController) {
		g_printf ("Controller Ok :-)\n");
	} else {
		g_printf ("Controller Failed :-(\n");
	}
}

int
main (int argc, char *argv[])
{
// 	gtk_init (&argc, &argv);
// 	app_init ();
// 	gtk_main ();

	return MApplicationMain ("MoccaTest", "0.1",
				 argc, argv,
				 "./mabout.mic");
}
