#include "main.h"

#include <Mocca/Models/MInterface.h>
#include <glib.h>

void test_mid (void)
{
	MID *mid = NULL;
	
	g_printf ("Starting MID test.\n");

	g_printf ("Creating the object ");
	mid = [[MID alloc] init];
	if (mid != NULL) {
		g_printf ("Ok.\n");
	} else {
		g_printf ("Failed.\n");
		return;
	}

	g_printf ("Setting a new uri.\n");
	[mid setKey: "uri" withValue: (id) g_strdup ("file://tmp/mocca.id")];
	g_printf ("The uri is: %s\n", [mid uri]);

	g_printf ("Loading uri... ");
	if ([mid load]) {
		g_printf ("Ok.\n");
	} else {
		g_printf ("Failed.\n");
	}

	g_printf ("Free the object.\n");
	[mid free];
}
