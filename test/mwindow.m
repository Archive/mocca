#include "main.h"
#include "myWindowController.h"

#include <glib.h>

void test_mwindow (void)
{
	myWindowController *controller = NULL;
	
	g_printf ("Starting MWindow test.\n");

	g_printf ("Creating the window using a controller ... ");
	controller = [[myWindowController alloc] init];
	if (controller != NULL) {
		g_printf ("Ok.\n");
	} else {
		g_printf ("Failed.\n");
		return;
	}
}
