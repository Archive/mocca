/*
 * MKeyValue.h
 *
 * Author: �lvaro Pe�a <alvaropg@telefonica.net>
 *
 */

#ifndef __MKEYVALUE_H__
#define __MKEYVALUE_H__

@protocol MKeyValue

- (id) getKey: (char *) key;
- (void) setKey: (char *) key withValue: (id) value;

@end

#endif /* __MKEYVALUE_H__ */
