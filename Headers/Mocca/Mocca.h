/*
 * Mocca.h
 *
 * Main include file for Mocca Library
 *
 * Author: �lvaro Pe�a <alvaropg@telefonica.net>
 *
 */

#ifndef __MOCCA_H__
#define __MOCCA_H__

#include <Mocca/MApplication.h>

/* Models */
#include <Mocca/Models/MModel.h>

/* Views */
#include <Mocca/Views/MView.h>
#include <Mocca/Views/MWindow.h>

/* Controllers */
#include <Mocca/Controllers/MController.h>
#include <Mocca/Controllers/MInterfaceController.h>

#endif /* __MOCCA_H__ */
