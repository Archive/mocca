/* MAplication
 * Copyright (C) 2005 GNOME Foundation
 *
 * AUTHORS:
 *      Álvaro Peña <apg@openshine.com>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MAPPLICATION_H__
#define __MAPPLICATION_H__

#include <objc/Object.h>
#include <glib.h>
#include <Mocca/Controllers/MInterfaceController.h>

gint MApplicationMain (const gchar *appId, const gchar *appVersion, gint nArgs, gchar *args[], const gchar *micPath);

@interface MApplication: Object
{
	MInterfaceController *_iController;
}

- init;
- (void) quit;

- (MInterfaceController *) iController;
- (void) setIController: (MInterfaceController *) iController;

@end

MApplication *mApp;

#endif /* __MAPPLICATION_H__ */
